import { SaveAddressPage } from './../pages/save-address/save-address';
import { MapPage } from './../pages/map/map';
import { ViewFeedbackPage } from './../pages/view-feedback/view-feedback';
import { DoctorRecordsRepositoryPage } from './../pages/doctor-records-repository/doctor-records-repository';
import { PatientRecordsRepositoryPage } from './../pages/patient-records-repository/patient-records-repository';
import { ViewDoctorPrescriptionPage } from './../pages/view-doctor-prescription/view-doctor-prescription';
import { ViewPatientPrescriptionPage } from './../pages/view-patient-prescription/view-patient-prescription';
import { DoctorPrescriptionsPage } from './../pages/doctor-prescriptions/doctor-prescriptions';
import { PatientPrescriptionsPage } from './../pages/patient-prescriptions/patient-prescriptions';
import { ViewDoctorAppointmentsPage } from './../pages/view-doctor-appointments/view-doctor-appointments';
import { ViewPatientAppointmentsPage } from './../pages/view-patient-appointments/view-patient-appointments';
import { ViewDoctorProfilePage } from './../pages/view-doctor-profile/view-doctor-profile';
import { DoctorsListPage } from './../pages/doctors-list/doctors-list';
import { AppInfoPage } from './../pages/app-info/app-info';
import { SendFeedbackPage } from './../pages/send-feedback/send-feedback';
import { GetAppointmentPage } from './../pages/get-appointment/get-appointment';
import { HealthTipsPage } from './../pages/health-tips/health-tips';
import { PatientsAppointmentsPage } from './../pages/patients-appointments/patients-appointments';
import { PatientsReportRepositoryPage } from './../pages/patients-report-repository/patients-report-repository';
import { DoctorsReportRepositoryPage } from './../pages/doctors-report-repository/doctors-report-repository';
import { ManageFeedbacksPage } from './../pages/manage-feedbacks/manage-feedbacks';
import { PatientsHistoryPage } from './../pages/patients-history/patients-history';
import { AddPrescriptionsPage } from './../pages/add-prescriptions/add-prescriptions';
import { DoctorsAppointmentsPage } from './../pages/doctors-appointments/doctors-appointments';
import { ForgotPasswordPage } from './../pages/forgot-password/forgot-password';
import { ProfilePage } from './../pages/profile/profile';
import { PopoverPage } from './../pages/popover/popover';
import { HomePatientPage } from './../pages/home-patient/home-patient';
import { HomeDoctorPage } from './../pages/home-doctor/home-doctor';
import { RegisterPage } from './../pages/register/register';
import { LoginPage } from './../pages/login/login';
import { MainPage } from './../pages/main/main';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import * as firebase from "firebase/app";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireStorageModule  } from 'angularfire2/storage';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DatabaseProvider } from '../providers/database/database';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { HttpModule } from '@angular/http';
import { CallNumber } from '@ionic-native/call-number';
import { Toast } from '@ionic-native/toast';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { GoogleMaps } from "@ionic-native/google-maps";
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';

var config = {
  apiKey: "AIzaSyAKu4-aG66GypGFgHVgZqBlnVoiq9nX7RY",
  authDomain: "doctorsapp-a1deb.firebaseapp.com",
  databaseURL: "https://doctorsapp-a1deb.firebaseio.com",
  projectId: "doctorsapp-a1deb",
  storageBucket: "doctorsapp-a1deb.appspot.com",
  messagingSenderId: "444888513857"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    MainPage,
    LoginPage,
    RegisterPage,
    HomeDoctorPage,
    HomePatientPage,
    PopoverPage,
    ProfilePage,
    ForgotPasswordPage,
    DoctorsAppointmentsPage,
    PatientsHistoryPage,
    ManageFeedbacksPage,
    DoctorsReportRepositoryPage,
    PatientsReportRepositoryPage,
    PatientsAppointmentsPage,
    HealthTipsPage,
    AppInfoPage,
    DoctorsListPage,
    ViewDoctorProfilePage,
    ViewPatientAppointmentsPage,
    ViewDoctorAppointmentsPage,
    PatientPrescriptionsPage,
    DoctorPrescriptionsPage,
    ViewPatientPrescriptionPage,
    ViewDoctorPrescriptionPage,
    PatientRecordsRepositoryPage,
    DoctorRecordsRepositoryPage,
    ViewFeedbackPage,
    MapPage,
    SaveAddressPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollAssist: false,
      autoFocusAssist: false,
      tabsHideOnSubPages: true,
    }),
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
    HttpModule,
    AngularFireStorageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    MainPage,
    LoginPage,
    RegisterPage,
    HomeDoctorPage,
    HomePatientPage,
    PopoverPage,
    ProfilePage,
    ForgotPasswordPage,
    DoctorsAppointmentsPage,
    PatientsHistoryPage,
    ManageFeedbacksPage,
    DoctorsReportRepositoryPage,
    PatientsReportRepositoryPage,
    PatientsAppointmentsPage,
    HealthTipsPage,
    AppInfoPage,
    DoctorsListPage,
    ViewDoctorProfilePage,
    ViewPatientAppointmentsPage,
    ViewDoctorAppointmentsPage,
    PatientPrescriptionsPage,
    DoctorPrescriptionsPage,
    ViewPatientPrescriptionPage,
    ViewDoctorPrescriptionPage,
    PatientRecordsRepositoryPage,
    DoctorRecordsRepositoryPage,
    ViewFeedbackPage,
    MapPage,
    SaveAddressPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    DatabaseProvider,
    Camera,
    PhotoViewer,
    CallNumber,
    Toast,
    FileChooser,
    FilePath,
    File,
    Geolocation,
    Network,
    GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
