import { HomePatientPage } from './../pages/home-patient/home-patient';
import { HomeDoctorPage } from './../pages/home-doctor/home-doctor';
import { MainPage } from './../pages/main/main';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = MainPage;
  rootPage: any;

  isLoggedIn: any;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public storage: Storage) {

    this.checkUserLoggedIn();
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  checkUserLoggedIn()
  {
    this.storage.get('isUserLoggedIn').then((val) => {
      console.log("IsLoggedIn 1: ", val);
      if (val != 'true')
      {
        this.isLoggedIn = "false";
        console.log("IsLoggedIn 2: ", this.isLoggedIn);
        this.checkPreviousAuthorization(this.isLoggedIn, "notLoggedIn");
      }
      else
      {
        this.isLoggedIn = val;
        console.log("IsLoggedIn 3: ", this.isLoggedIn);
        this.storage.get('usertype').then((type) => {
          console.log("Logged In UserType: ",type);
          this.checkPreviousAuthorization(this.isLoggedIn, type);
        });
      }
    });
  }

  checkPreviousAuthorization(isLoggedIn, userType)
  {
    if (isLoggedIn == "true")
    {
      if(userType == "Doctor")
      {
        this.rootPage = HomeDoctorPage;
      }
      else if(userType == "Patient")
      {
        this.rootPage = HomePatientPage;
      }
    }
    else
    {
      this.rootPage = MainPage;
    }
  }
}
