import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";
import * as firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';

@Injectable()
export class DatabaseProvider {

  reports: any;


  constructor(public http: Http,
              public db: AngularFireDatabase,
              public afStorage: AngularFireStorage){

                // this.reports = firebase.database().ref('/reports');
  }

  uploadDisplayImage(imageString): Promise<any>
  {
    let image         : string = 'user-' + new Date().getTime() + '.jpg',
        storageRef    : any,
        parseUpload   : any;


    return new Promise((resolve, reject) => {
      storageRef  = firebase.storage().ref('profilePictures/' + image);
      parseUpload = storageRef.putString(imageString, 'data_url');

      parseUpload.on('state_changed', (_snapshot) => {

      },
      (_err) =>
      {
        reject(_err);
      },
      (success) =>
      {
        resolve(parseUpload.snapshot);
      });
    });
  }

  pushUpload(user, upload, filename): Promise<any>
  {
    let file_name     : string = user + new Date().getTime() + filename,
        storageRef    : any,
        uploadTask    : any;

    return new Promise((resolve, reject) => {
      storageRef  = firebase.storage().ref('reports/' + file_name);
      uploadTask = storageRef.put(upload);

      uploadTask.on('state_changed', (snapshot) => {
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      },
      (_error) => {
        reject(_error);
        alert(_error);
      },
      (success) => {
        uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
          console.log('File available at', downloadURL);
          let data = [];
          data.push({
            dbFilename: file_name,
            downloadURL: downloadURL
          });
          resolve(data);
        });
      })
    });
  }

  deleteFileStorage(dbfilename)
  {
    let storageRef = firebase.storage().ref();
    storageRef.child('reports/'+ dbfilename).delete();
  }

  makeFileIntoBlob(_imagePath, name, type)
  {
    return new Promise((resolve, reject) => {
      (window as any).resolveLocalFileSystemURL(_imagePath, (fileEntry) => {

        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], {type: type});
            imgBlob.name = name;
            resolve(imgBlob);
          };

          reader.onerror = (e) => {
            alert('Failed file read '+ e.toString());
            reject(e);
          };

          reader.readAsArrayBuffer(resFile);
        });
      });
    });
  }

  getFileName(filestring)
  {
    let file
    file = filestring.replace(/^.*[\\\/]/, '')
    return file;
  }

  getFileExt(filestring)
  {
    let file = filestring.substr(filestring.lastIndexOf('.') + 1);
    return file;
  }

  getRequestFiles(): any
  {
    return this.reports.child('reports');
  }

  addAssignmentFile(file: any): any
  {
    return this.reports.child(file.filename).put(file.blob, {contentType: file.type}).then((savedFile) => {
      this.reports.child('reports').push({
        file: savedFile.downloadURL,
        name: file.filename,
        ext: file.fileext,
        type: file.type
      });
    })
  }

}
