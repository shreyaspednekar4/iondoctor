import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPatientPrescriptionPage } from './view-patient-prescription';

@NgModule({
  declarations: [
    ViewPatientPrescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewPatientPrescriptionPage),
  ],
})
export class ViewPatientPrescriptionPageModule {}
