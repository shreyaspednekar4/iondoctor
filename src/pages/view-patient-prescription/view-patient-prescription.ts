import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-view-patient-prescription',
  templateUrl: 'view-patient-prescription.html',
})
export class ViewPatientPrescriptionPage {

  public appointment: any;
  public prescriptions: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public callNumber: CallNumber,
              public toast: Toast) {

    this.appointment = navParams.get('appointment');
    this.getPrescriptions(this.appointment.key);
  }

  getPrescriptions(appkey)
  {
    var ref = firebase.database().ref("prescriptions");
    ref.orderByChild("appointmentkey").equalTo(appkey).once("value", (items : any) => {
      let prescriptions : any = [];
      items.forEach((item) => {
        prescriptions.push({
          key             : item.key,
          appointmentkey  : item.val().appointmentkey,
          medicine        : item.val().medicine,
          dosage          : item.val().dosage
        });
      });
      this.prescriptions = prescriptions;
      console.log(this.prescriptions);
    });
  }

  callNow(appointment)
  {
    console.log(appointment.doctordetails.contact);
    if(appointment.doctordetails.contact != null)
    {
      this.callNumber.callNumber(appointment.doctordetails.contact, true)
      .then(res => {
        console.log("Launched dialer!", res);
      })
      .catch(err => {
        console.log("Error launching dialer!", err);
      });
    }
    else
    {
      this.toast.show('No contact found!','3000','center').subscribe();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPatientPrescriptionPage');
    this.getPrescriptions(this.appointment.key);
  }

  ionViewWillEnter(){
    this.getPrescriptions(this.appointment.key);
  }

}
