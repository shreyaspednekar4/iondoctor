import { MapPage } from './../map/map';
import { PatientRecordsRepositoryPage } from './../patient-records-repository/patient-records-repository';
import { PatientPrescriptionsPage } from './../patient-prescriptions/patient-prescriptions';
import { DoctorsListPage } from './../doctors-list/doctors-list';
import { GetAppointmentPage } from './../get-appointment/get-appointment';
import { HealthTipsPage } from './../health-tips/health-tips';
import { PatientsReportRepositoryPage } from './../patients-report-repository/patients-report-repository';
import { PatientsAppointmentsPage } from './../patients-appointments/patients-appointments';
import { ProfilePage } from './../profile/profile';
import { PopoverPage } from './../popover/popover';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home-patient',
  templateUrl: 'home-patient.html',
})
export class HomePatientPage {

  userEmail: any;
  userKey: any;
  userData: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              public storage: Storage) {

                this.getUserKey();
  }

  openPopover(myEvent)
  {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.userKey = val;
      console.log("Storage Key: ",this.userKey);
      this.getUserData(this.userKey);
    });
  }

  getUserData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
	      password        : item.val().password,
	      profile_picture : item.val().profile_picture
      })
      this.userData = user;
    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getUserKey();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  gotoProfile()
  {
    this.navCtrl.push(ProfilePage);
  }

  gotoDoctorsList()
  {
    this.navCtrl.push(DoctorsListPage);
  }

  gotoMyAppointments()
  {
    this.navCtrl.push(PatientsAppointmentsPage);
  }

  gotoViewPrescription()
  {
    this.navCtrl.push(PatientPrescriptionsPage);
  }

  gotoReportRepository()
  {
    this.navCtrl.push(PatientRecordsRepositoryPage);
  }

  gotoGetAppointment()
  {
    //this.navCtrl.push(GetAppointmentPage);
    this.navCtrl.push(DoctorsListPage);
  }

  gotoHealthTips()
  {
    this.navCtrl.push(HealthTipsPage);
  }

  openMap()
  {
    this.navCtrl.push(MapPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePatientPage');
    this.getUserKey();
  }

  ionViewWillEnter(){
    this.getUserKey();
  }

}
