import { PatientsReportRepositoryPage } from './../patients-report-repository/patients-report-repository';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";

@IonicPage()
@Component({
  selector: 'page-patient-records-repository',
  templateUrl: 'patient-records-repository.html',
})
export class PatientRecordsRepositoryPage {

  userkey: any;
  reports: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              public toastCtrl: ToastController) {

                this.getUserKey();
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.userkey = val;
      //console.log("Storage Key: ",this.userkey);
      this.getReports(this.userkey);
    });
  }

  getReports(userkey)
  {
    var ref = firebase.database().ref('reports');
    ref.orderByChild("userkey").equalTo(userkey).once("value", (items : any) => {
      let reports = [];
      items.forEach((item) => {
        reports.push({
          key             : item.key,
          userkey         : item.val().userkey,
          doctorkey       : item.val().doctorkey,
          downloadURL     : item.val().downloadURL,
          filename        : item.val().filename,
          dbFilename      : item.val().dbFilename,
          patientrelation : item.val().patientrelation,
          patientname     : item.val().patientname,
          recordAddedBy   : item.val().recordAddedBy,
          recordtype      : item.val().recordtype,
          recorddate      : item.val().recorddate,
          userdetails: {
            fullname        : item.val().userdetails.fullname,
            contact         : item.val().userdetails.contact,
            email           : item.val().userdetails.email,
            age             : item.val().userdetails.age,
            gender          : item.val().userdetails.gender,
            profile_picture : item.val().userdetails.profile_picture,
            address: {
              addline1      : item.val().userdetails.address.addline1,
              addline2      : item.val().userdetails.address.addline2,
              country       : item.val().userdetails.address.country,
              state         : item.val().userdetails.address.state,
              city          : item.val().userdetails.address.city,
              postal_code   : item.val().userdetails.address.postal_code
            }
          },
          doctordetails: {
            fullname        : item.val().doctordetails.fullname,
            contact         : item.val().doctordetails.contact,
            email           : item.val().doctordetails.email,
            age             : item.val().doctordetails.age,
            gender          : item.val().doctordetails.gender,
            profile_picture : item.val().doctordetails.profile_picture,
            address: {
              addline1      : item.val().doctordetails.address.addline1,
              addline2      : item.val().doctordetails.address.addline2,
              country       : item.val().doctordetails.address.country,
              state         : item.val().doctordetails.address.state,
              city          : item.val().doctordetails.address.city,
              postal_code   : item.val().doctordetails.address.postal_code
            },
            clinicdetails   : {
              clinicname    : item.val().doctordetails.clinicdetails.clinicname,
              education     : item.val().doctordetails.clinicdetails.education,
              department    : item.val().doctordetails.clinicdetails.department,
              speciality    : item.val().doctordetails.clinicdetails.speciality,
              experience    : item.val().doctordetails.clinicdetails.experience,
              fees          : item.val().doctordetails.clinicdetails.fees,
              clinictimefrom: item.val().doctordetails.clinicdetails.clinictimefrom,
              clinictimeto  : item.val().doctordetails.clinicdetails.clinictimeto
            }
          }
        });
      });
      var obj = {};
      for(var i = 0; i < reports.length; i++)
      {
        obj[reports[i]['doctorkey']] = reports[i];
      }
      let doctorfilter: any = [];
      for(var key in obj)
      {
        doctorfilter.push(obj[key]);
      }
      this.reports = doctorfilter;
      console.log(this.reports);
    })
  }

  viewRecords(report)
  {
    console.log(report.doctorkey);
    this.navCtrl.push(PatientsReportRepositoryPage, {doctorkey: report.doctorkey});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientRecordsRepositoryPage');
    this.getUserKey();
  }

}
