import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientRecordsRepositoryPage } from './patient-records-repository';

@NgModule({
  declarations: [
    PatientRecordsRepositoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientRecordsRepositoryPage),
  ],
})
export class PatientRecordsRepositoryPageModule {}
