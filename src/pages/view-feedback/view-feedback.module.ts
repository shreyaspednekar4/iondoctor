import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewFeedbackPage } from './view-feedback';

@NgModule({
  declarations: [
    ViewFeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewFeedbackPage),
  ],
})
export class ViewFeedbackPageModule {}
