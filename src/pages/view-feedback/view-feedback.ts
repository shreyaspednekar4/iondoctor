import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-view-feedback',
  templateUrl: 'view-feedback.html',
})
export class ViewFeedbackPage {

  public feedback: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {

                this.feedback = navParams.get('feedback');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewFeedbackPage');
  }

}
