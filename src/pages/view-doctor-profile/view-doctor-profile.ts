import { PatientsReportRepositoryPage } from './../patients-report-repository/patients-report-repository';
import { SendFeedbackPage } from './../send-feedback/send-feedback';
import { GetAppointmentPage } from './../get-appointment/get-appointment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-view-doctor-profile',
  templateUrl: 'view-doctor-profile.html',
})
export class ViewDoctorProfilePage {

  public user: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public photoViewer: PhotoViewer,
              public alertCtrl: AlertController,
              public callNumber: CallNumber,
              public modalCtrl: ModalController) {

                this.user = navParams.get('user');
  }

  showPicture(user)
  {
    console.log(user.profile_picture);
    this.photoViewer.show(user.profile_picture, user.fullname, {share: true});
  }

  call()
  {
    console.log(this.user.contact);
    if(this.user.contact != null)
    {
      this.callNumber.callNumber(this.user.contact, true)
      .then(res => {
        console.log("Launched dialer!", res);
      })
      .catch(err => {
        console.log("Error launching dialer!", err);
      });
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Oops',
        message: 'No contact found!',
        buttons: [
          {
            text: 'Ok'
          }
        ]
      });
      alert.present();
    }
  }

  gotoGetAppointment()
  {
    //this.navCtrl.push(GetAppointmentPage, {doctor: this.user});

    let modal = this.modalCtrl.create('GetAppointmentPage', {doctor: this.user});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  uploadReports()
  {
    this.navCtrl.push(PatientsReportRepositoryPage, {doctorkey: this.user.key});
  }

  gotoSendFeedback()
  {
    //this.navCtrl.push(SendFeedbackPage, {doctor: this.user});

    let modal = this.modalCtrl.create('SendFeedbackPage', {doctor: this.user});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewDoctorProfilePage');
  }

}
