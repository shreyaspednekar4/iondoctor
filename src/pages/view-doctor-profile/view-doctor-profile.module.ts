import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewDoctorProfilePage } from './view-doctor-profile';

@NgModule({
  declarations: [
    ViewDoctorProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewDoctorProfilePage),
  ],
})
export class ViewDoctorProfilePageModule {}
