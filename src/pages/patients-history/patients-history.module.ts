import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsHistoryPage } from './patients-history';

@NgModule({
  declarations: [
    PatientsHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientsHistoryPage),
  ],
})
export class PatientsHistoryPageModule {}
