import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeDoctorPage } from './home-doctor';

@NgModule({
  declarations: [
    HomeDoctorPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeDoctorPage),
  ],
})
export class HomeDoctorPageModule {}
