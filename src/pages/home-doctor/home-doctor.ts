import { MapPage } from './../map/map';
import { DoctorRecordsRepositoryPage } from './../doctor-records-repository/doctor-records-repository';
import { DoctorPrescriptionsPage } from './../doctor-prescriptions/doctor-prescriptions';
import { HealthTipsPage } from './../health-tips/health-tips';
import { DoctorsReportRepositoryPage } from './../doctors-report-repository/doctors-report-repository';
import { ManageFeedbacksPage } from './../manage-feedbacks/manage-feedbacks';
import { PatientsHistoryPage } from './../patients-history/patients-history';
import { AddPrescriptionsPage } from './../add-prescriptions/add-prescriptions';
import { DoctorsAppointmentsPage } from './../doctors-appointments/doctors-appointments';
import { ProfilePage } from './../profile/profile';
import { PopoverPage } from './../popover/popover';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home-doctor',
  templateUrl: 'home-doctor.html',
})
export class HomeDoctorPage {

  userEmail: any;
  userKey: any;
  userData: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              public storage: Storage) {

                this.getUserKey();
  }

  openPopover(myEvent)
  {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.userKey = val;
      console.log("Storage Key: ",this.userKey);
      this.getUserData(this.userKey);
    });
  }

  getUserData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        },
        doctordetails   : {
          clinicname    : item.val().doctordetails.clinicname,
          education     : item.val().doctordetails.education,
          department    : item.val().doctordetails.department,
          speciality    : item.val().doctordetails.speciality,
          experience    : item.val().doctordetails.experience,
          fees          : item.val().doctordetails.fees,
          clinictimefrom: item.val().doctordetails.clinictimefrom,
          clinictimeto  : item.val().doctordetails.clinictimeto
        }
      })
      this.userData = user;
    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getUserKey();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  gotoProfile()
  {
    this.navCtrl.push(ProfilePage);
  }

  gotoMyAppointments()
  {
    this.navCtrl.push(DoctorsAppointmentsPage);
  }

  gotoAddPrescription()
  {
    this.navCtrl.push(DoctorPrescriptionsPage);
  }

  gotoReportRepository()
  {
    this.navCtrl.push(DoctorRecordsRepositoryPage);
  }

  gotoPatientsHistory()
  {
    this.navCtrl.push(PatientsHistoryPage);
  }

  gotoManageFeedbacks()
  {
    this.navCtrl.push(ManageFeedbacksPage);
  }

  gotoHealthTips()
  {
    this.navCtrl.push(HealthTipsPage);
  }

  openMap()
  {
    this.navCtrl.push(MapPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeDoctorPage');
    this.getUserKey();
  }

  ionViewWillEnter(){
    this.getUserKey();
   }

}
