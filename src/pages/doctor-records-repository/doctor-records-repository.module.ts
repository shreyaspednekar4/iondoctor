import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorRecordsRepositoryPage } from './doctor-records-repository';

@NgModule({
  declarations: [
    DoctorRecordsRepositoryPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorRecordsRepositoryPage),
  ],
})
export class DoctorRecordsRepositoryPageModule {}
