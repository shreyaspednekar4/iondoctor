import { SaveAddressPage } from './../save-address/save-address';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, CameraPosition, MarkerOptions, Marker, MyLocation, LocationService, Geocoder, GeocoderResult } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  dragAddress: any;
  currentLocation: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              public geolocation: Geolocation,
              public googleMaps: GoogleMaps,
              public toastCtrl: ToastController) {
  }

  loadMap()
  {
    this.geolocation.getCurrentPosition().then((position) => {

      let mapOptions: GoogleMapOptions = {
        camera: {
          target: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          },
          zoom: 18,
          tilt: 30
        },
      };

      this.map = GoogleMaps.create('map', mapOptions);

      Geocoder.geocode({
        'position': {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
      }).then((results: GeocoderResult[]) => {
        if(results.length == 0)
        {
          //Not found
          return null;
        }
        else
        {
          let address: any = [
            results[0].subThoroughfare || "",
            results[0].thoroughfare || "",
            results[0].locality || "",
            results[0].adminArea || "",
            results[0].postalCode || "",
            results[0].country || ""].join(", ");
          let toast = this.toastCtrl.create({
            message: "You're at " + address,
            duration: 5000
          });
          toast.present();
          let marker: Marker = this.map.addMarkerSync({
            title: address,
            icon: 'red',
            animation: 'DROP',
            position: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            },
            draggable: true
          });
          marker.showInfoWindow();

          let current : any = [];
          current.push({
            address: address,
            coords: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            }
          });
          this.currentLocation = current;
        }
      });

      // let dragMarker: Marker;
      // dragMarker.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe((params: any[]) => {
      //   dragMarker.setPosition(params[0]);
      //   dragMarker = this.map.addMarkerSync({
      //     icon: 'blue',
      //     animation: 'DROP',
      //     position: params[0]
      //   });
      //   this.getAddress(params[0]);
      //   dragMarker.setTitle(this.dragAddress as string);
      //   dragMarker.showInfoWindow();
      // });
    });
  }

  getAddress(position): string
  {
    let add: string;
    Geocoder.geocode({
      'position': position
    }).then((results: GeocoderResult[]) => {
      if(results.length == 0)
      {
        //Not found
        return null;
      }
      else
      {
        let address: any = [
          results[0].subThoroughfare || "",
          results[0].thoroughfare || "",
          results[0].locality || "",
          results[0].adminArea || "",
          results[0].postalCode || "",
          results[0].country || ""].join(", ");
          this.dragAddress = address;
      }
    });
    return this.dragAddress;
  }

  selectCurrentLocation()
  {
    this.navCtrl.push(SaveAddressPage);
    // let toast = this.toastCtrl.create({
    //   message: "Address: " + this.currentLocation.address + "Lat: " + this.currentLocation.coords.lat + "Lng: " + this.currentLocation.coords.lng,
    //   duration: 10000
    // });
    // toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.loadMap();
  }


}
