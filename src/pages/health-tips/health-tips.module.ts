import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HealthTipsPage } from './health-tips';

@NgModule({
  declarations: [
    HealthTipsPage,
  ],
  imports: [
    IonicPageModule.forChild(HealthTipsPage),
  ],
})
export class HealthTipsPageModule {}
