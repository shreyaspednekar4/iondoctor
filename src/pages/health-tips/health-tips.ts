import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-health-tips',
  templateUrl: 'health-tips.html',
})
export class HealthTipsPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public iab: InAppBrowser) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HealthTipsPage');
  }

}
