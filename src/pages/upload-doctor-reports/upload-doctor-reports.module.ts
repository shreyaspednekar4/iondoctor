import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadDoctorReportsPage } from './upload-doctor-reports';

@NgModule({
  declarations: [
    UploadDoctorReportsPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadDoctorReportsPage),
  ],
})
export class UploadDoctorReportsPageModule {}
