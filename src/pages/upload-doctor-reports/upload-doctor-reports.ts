import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ViewController } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { DatabaseProvider } from '../../providers/database/database';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {FileChooser} from '@ionic-native/file-chooser';
import {FilePath} from '@ionic-native/file-path';
import * as _ from "lodash";
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-upload-doctor-reports',
  templateUrl: 'upload-doctor-reports.html',
})
export class UploadDoctorReportsPage {

  authForm: FormGroup;
  userkey: any;
  userdata: any;
  doctorkey: any;
  doctordata: any;
  reports: any;
  selectedFiles: FileList;
  currentUpload: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dbProvider: DatabaseProvider,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public iab: InAppBrowser,
              public filePath: FilePath,
              public fileChooser: FileChooser,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public viewCtrl: ViewController) {

                this.userdata = navParams.get('user');
                console.log("user: ",this.userdata);
                this.getUserKey();

                this.authForm = formBuilder.group({
                  file: ['', Validators.compose([Validators.required])],
                  recordtype: ['', Validators.compose([Validators.required])],
                  recorddate: ['', Validators.compose([Validators.required])]
                });
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.doctorkey = val;
      //console.log("Storage Key: ",this.userkey);
      this.getDoctorData(this.doctorkey);
    });
  }

  getDoctorData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        },
        doctordetails   : {
          clinicname    : item.val().doctordetails.clinicname,
          education     : item.val().doctordetails.education,
          department    : item.val().doctordetails.department,
          speciality    : item.val().doctordetails.speciality,
          experience    : item.val().doctordetails.experience,
          fees          : item.val().doctordetails.fees,
          clinictimefrom: item.val().doctordetails.clinictimefrom,
          clinictimeto  : item.val().doctordetails.clinictimeto
        }
      })
      this.doctorkey = item.key;
      this.doctordata = user;
      console.log(this.doctordata);
    })
  }

  selectRelation(relation)
  {
    if(relation != "Myself")
    {
      this.authForm.controls["patientname"].reset();
    }
  }

  detectFiles(event)
  {
    this.selectedFiles = event.target.files;
  }

  uploadSingle()
  {
    let file = this.selectedFiles.item(0),
        recordtype   : string = this.authForm.controls["recordtype"].value,
        recorddate   : string = this.authForm.controls["recorddate"].value

    this.dbProvider.pushUpload(this.doctordata[0].fullname,file, file.name)
    .then((snapshot: any) => {
      let downloadURL: any = snapshot[0].downloadURL;
      let dbFilename: any = snapshot[0].dbFilename;
      console.log("snapshot: ",snapshot);
      this.saveReport({
        userkey: this.userdata[0].key,
        doctorkey: this.doctordata[0].key,
        filename: file.name,
        dbFilename: dbFilename,
        downloadURL: downloadURL,
        patientrelation: '',
        patientname: '',
        recordAddedBy: 'Dr. ' + this.doctordata[0].fullname,
        recordtype: recordtype,
        recorddate: recorddate,
        userdetails: {
          fullname: this.userdata[0].fullname,
          contact: this.userdata[0].contact,
          email: this.userdata[0].email,
          age: this.userdata[0].age,
          gender: this.userdata[0].gender,
          profile_picture: this.userdata[0].profile_picture,
          address: {
            addline1: this.userdata[0].address.addline1,
            addline2: this.userdata[0].address.addline2,
            country: this.userdata[0].address.country,
            state: this.userdata[0].address.state,
            city: this.userdata[0].address.city,
            postal_code: this.userdata[0].address.postal_code
          }
        },
        doctordetails: {
          fullname: this.doctordata[0].fullname,
	        contact: this.doctordata[0].contact,
	        email: this.doctordata[0].email,
          age: this.doctordata[0].age,
          gender: this.doctordata[0].gender,
          profile_picture: this.doctordata[0].profile_picture,
          address: {
            addline1: this.doctordata[0].address.addline1,
            addline2: this.doctordata[0].address.addline2,
            country: this.doctordata[0].address.country,
            state: this.doctordata[0].address.state,
            city: this.doctordata[0].address.city,
            postal_code: this.doctordata[0].address.postal_code
          },
          clinicdetails: {
            clinicname: this.doctordata[0].doctordetails.clinicname,
            education: this.doctordata[0].doctordetails.education,
            department: this.doctordata[0].doctordetails.department,
            speciality: this.doctordata[0].doctordetails.speciality,
            experience: this.doctordata[0].doctordetails.experience,
            fees: this.doctordata[0].doctordetails.fees,
            clinictimefrom: this.doctordata[0].doctordetails.clinictimefrom,
            clinictimeto: this.doctordata[0].doctordetails.clinictimeto
          }
        }
      });
      let toast = this.toastCtrl.create({
        message: 'Record uploaded successfully!',
        duration: 2000
      });
      toast.present();
      this.viewCtrl.dismiss();
    })
    .catch(error => {
      console.log(error);
    });
  }

  saveReport(repObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('reports');
	    updateRef.push(repObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  // uploadMulti() {
//   let files = this.selectedFiles
//   let filesIndex = _.range(files.length)
//   _.each(filesIndex, (idx) => {
//     this.currentUpload = new Upload(files[idx]);
//     this.dbProvider.pushUpload(this.currentUpload)}
//   )
// }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadDoctorReportsPage');
  }

}
