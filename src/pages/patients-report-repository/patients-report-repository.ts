import { UploadPatientReportsPage } from './../upload-patient-reports/upload-patient-reports';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { DatabaseProvider } from '../../providers/database/database';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {FileChooser} from '@ionic-native/file-chooser';
import {FilePath} from '@ionic-native/file-path';
import * as _ from "lodash";
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-patients-report-repository',
  templateUrl: 'patients-report-repository.html',
})
export class PatientsReportRepositoryPage {

  authForm: FormGroup;
  userkey: any;
  userdata: any;
  doctorkey: any;
  doctordata: any;
  reports: any;
  selectedFiles: FileList;
  currentUpload: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dbProvider: DatabaseProvider,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public iab: InAppBrowser,
              public filePath: FilePath,
              public fileChooser: FileChooser,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public modalCtrl: ModalController) {

                this.doctorkey = navParams.get('doctorkey');
                this.getDoctorData(this.doctorkey);
                this.getUserKey();

                // this.authForm = formBuilder.group({
                //   file: ['', Validators.compose([Validators.required])],
                //   patientrelation: ['', Validators.compose([Validators.required])],
                //   patientname: ['', Validators.compose([Validators.required])],
                //   recordtype: ['', Validators.compose([Validators.required])],
                //   recorddate: ['', Validators.compose([Validators.required])]
                // });

  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.userkey = val;
      //console.log("Storage Key: ",this.userkey);
      this.getUserData(this.userkey);
      this.getReports(this.userkey);
    });
  }

  getUserData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        },
        doctordetails   : {
          clinicname    : item.val().doctordetails.clinicname,
          education     : item.val().doctordetails.education,
          department    : item.val().doctordetails.department,
          speciality    : item.val().doctordetails.speciality,
          experience    : item.val().doctordetails.experience,
          fees          : item.val().doctordetails.fees,
          clinictimefrom: item.val().doctordetails.clinictimefrom,
          clinictimeto  : item.val().doctordetails.clinictimeto
        }
      })
      this.userkey = item.key;
      this.userdata = user;
      console.log(this.userdata);
    })
  }

  getDoctorData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        },
        doctordetails   : {
          clinicname    : item.val().doctordetails.clinicname,
          education     : item.val().doctordetails.education,
          department    : item.val().doctordetails.department,
          speciality    : item.val().doctordetails.speciality,
          experience    : item.val().doctordetails.experience,
          fees          : item.val().doctordetails.fees,
          clinictimefrom: item.val().doctordetails.clinictimefrom,
          clinictimeto  : item.val().doctordetails.clinictimeto
        }
      })
      this.doctorkey = item.key;
      this.doctordata = user;
      console.log("Doctor: ",this.doctordata);
    })
  }

  getReports(userkey)
  {
    var ref = firebase.database().ref('reports');
    ref.orderByChild("userkey").equalTo(userkey).once("value", (items : any) => {
      let reports = [];
      items.forEach((item) => {
        if(item.val().doctorkey == this.doctorkey)
        {
          reports.push({
            key             : item.key,
            userkey         : item.val().userkey,
            doctorkey       : item.val().doctorkey,
            downloadURL     : item.val().downloadURL,
            filename        : item.val().filename,
            dbFilename      : item.val().dbFilename,
            patientrelation : item.val().patientrelation,
            patientname     : item.val().patientname,
            recordAddedBy   : item.val().recordAddedBy,
            recordtype      : item.val().recordtype,
            recorddate      : item.val().recorddate,
            userdetails: {
              fullname        : item.val().userdetails.fullname,
	            contact         : item.val().userdetails.contact,
	            email           : item.val().userdetails.email,
              age             : item.val().userdetails.age,
              gender          : item.val().userdetails.gender,
              profile_picture : item.val().userdetails.profile_picture,
              address: {
                addline1      : item.val().userdetails.address.addline1,
                addline2      : item.val().userdetails.address.addline2,
                country       : item.val().userdetails.address.country,
                state         : item.val().userdetails.address.state,
                city          : item.val().userdetails.address.city,
                postal_code   : item.val().userdetails.address.postal_code
              }
            },
            doctordetails: {
              fullname        : item.val().doctordetails.fullname,
	            contact         : item.val().doctordetails.contact,
	            email           : item.val().doctordetails.email,
              age             : item.val().doctordetails.age,
              gender          : item.val().doctordetails.gender,
              profile_picture : item.val().doctordetails.profile_picture,
              address: {
                addline1      : item.val().doctordetails.address.addline1,
                addline2      : item.val().doctordetails.address.addline2,
                country       : item.val().doctordetails.address.country,
                state         : item.val().doctordetails.address.state,
                city          : item.val().doctordetails.address.city,
                postal_code   : item.val().doctordetails.address.postal_code
              },
              clinicdetails   : {
                clinicname    : item.val().doctordetails.clinicdetails.clinicname,
                education     : item.val().doctordetails.clinicdetails.education,
                department    : item.val().doctordetails.clinicdetails.department,
                speciality    : item.val().doctordetails.clinicdetails.speciality,
                experience    : item.val().doctordetails.clinicdetails.experience,
                fees          : item.val().doctordetails.clinicdetails.fees,
                clinictimefrom: item.val().doctordetails.clinicdetails.clinictimefrom,
                clinictimeto  : item.val().doctordetails.clinicdetails.clinictimeto
              }
            }
          });
        }
      });
      this.reports = reports;
      console.log(this.reports);
    })
  }

  gotoFile(file)
  {
    this.iab.create(file,"_system", "location=yes");
  }

  addRecords()
  {
    let modal = this.modalCtrl.create('UploadPatientReportsPage', {doctor: this.doctordata});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  delete(file)
  {
    console.log(file.key);
    let confirm = this.alertCtrl.create({
      title: 'Delete Record',
        message: 'Are you sure want to delete ' + file.filename + ' record?',
        buttons: [
          {
            text: 'NO',
          },
          {
            text: 'YES',
            handler: () => {
            console.log('YES clicked');
            this.deleteFile(file);
            let toast = this.toastCtrl.create({
              message: 'Record deleted successfully!',
              duration: 2000
            });
            toast.present();
          }
        }
      ]
    });
    confirm.present();
  }

  deleteFile(file)
  {
    console.log(file.key);
    this.deleteFileData(file.key)
    .then(() => {
      this.dbProvider.deleteFileStorage(file.dbFilename);
      this.getReports(this.userkey);
    })
    .catch(error => console.log(error));
  }

  deleteFileData(filekey) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      let ref = firebase.database().ref('reports');
      ref.child(filekey).remove();
      resolve(true);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientsReportRepositoryPage');
    this.getUserKey();
    let toast = this.toastCtrl.create({
      message: 'Click on file to download! Slide right to delete!',
      duration: 2000,
      position: 'center'
    });
    toast.present();
    this.getDoctorData(this.doctorkey);
  }

  ionViewWillEnter(){
    this.getReports(this.userkey);
    this.getDoctorData(this.doctorkey);
  }

}
