import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsReportRepositoryPage } from './patients-report-repository';

@NgModule({
  declarations: [
    PatientsReportRepositoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientsReportRepositoryPage),
  ],
})
export class PatientsReportRepositoryPageModule {}
