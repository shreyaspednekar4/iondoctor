import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireList } from "angularfire2/database";

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  authForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController) {

                this.authForm = formBuilder.group({
                  email: ['', Validators.compose([Validators.required])]
                });
  }

  forgotPasswordClick()
  {
    let email: string = this.authForm.controls["email"].value;
    this.resetPassword(email);
  }

  resetPassword(Email)
  {
    firebase.auth().sendPasswordResetEmail(Email).then(function () {
      alert("Check your mail to reset your password!");
    }).catch(function () {
      alert("Invalid email!");
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

}
