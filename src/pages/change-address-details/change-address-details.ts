import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireList } from "angularfire2/database";
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-change-address-details',
  templateUrl: 'change-address-details.html',
})
export class ChangeAddressDetailsPage {

  authForm: FormGroup;

  public user: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public toast: Toast,
              public viewCtrl: ViewController) {

                this.user = navParams.get('user');

                this.authForm = formBuilder.group({
                  addline1: ['', Validators.compose([Validators.required])],
                  addline2: [''],
                  country: ['', Validators.compose([Validators.required])],
                  state: ['', Validators.compose([Validators.required])],
                  city: ['', Validators.compose([Validators.required])],
                  postCode: ['', Validators.compose([Validators.required])]
              });
  }

  save()
  {
    let addline1   : string = this.authForm.controls["addline1"].value,
        addline2   : string = this.authForm.controls["addline2"].value,
        country   : string = this.authForm.controls["country"].value,
        state   : string = this.authForm.controls["state"].value,
        city   : string = this.authForm.controls["city"].value,
        postCode   : string = this.authForm.controls["postCode"].value

        this.updateUser(this.user['key'],
        {
          address: {
            addline1: addline1,
            addline2: addline2,
            country: country,
            state: state,
            city: city,
            postal_code: postCode
          }
        });

        //this.toastCtrl.create({message: 'Address updated successfully!', duration: 3000}).present();
        this.toast.show('Address updated successfully!','3000','center').subscribe();
        this.viewCtrl.dismiss();
  }

  updateUser(id, userObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('users').child(id);
	    updateRef.update(userObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeAddressDetailsPage');
  }

}
