import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeAddressDetailsPage } from './change-address-details';

@NgModule({
  declarations: [
    ChangeAddressDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeAddressDetailsPage),
  ],
})
export class ChangeAddressDetailsPageModule {}
