import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPrescriptionsPage } from './add-prescriptions';

@NgModule({
  declarations: [
    AddPrescriptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPrescriptionsPage),
  ],
})
export class AddPrescriptionsPageModule {}
