import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-add-prescriptions',
  templateUrl: 'add-prescriptions.html',
})
export class AddPrescriptionsPage {

  authForm: FormGroup;

  appointment: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public toast: Toast,
              public viewCtrl: ViewController) {

                this.appointment = navParams.get('appointment');

                this.authForm = formBuilder.group({
                  medicine: ['', Validators.compose([Validators.required])],
                  dosage: ['']
                });
  }

  submit()
  {
    let medicine   : string = this.authForm.controls["medicine"].value,
        dosage   : string = this.authForm.controls["dosage"].value

        this.savePrescriptions({
          appointmentkey: this.appointment.key,
          medicine: medicine,
          dosage: dosage
        });
        this.toast.show('Prescription added!','3000','center').subscribe();
        this.viewCtrl.dismiss();
  }

  savePrescriptions(appObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('prescriptions');
	    updateRef.push(appObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPrescriptionsPage');
  }

}
