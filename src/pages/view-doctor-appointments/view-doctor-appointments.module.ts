import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewDoctorAppointmentsPage } from './view-doctor-appointments';

@NgModule({
  declarations: [
    ViewDoctorAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewDoctorAppointmentsPage),
  ],
})
export class ViewDoctorAppointmentsPageModule {}
