import { DoctorsReportRepositoryPage } from './../doctors-report-repository/doctors-report-repository';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-view-doctor-appointments',
  templateUrl: 'view-doctor-appointments.html',
})
export class ViewDoctorAppointmentsPage {

  public appointment: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public callNumber: CallNumber,
              public toast: Toast) {

                this.appointment = navParams.get('appointment');
  }

  callNow(appointment)
  {
    console.log(appointment.userdetails.contact);
    if(appointment.userdetails.contact != null)
    {
      this.callNumber.callNumber(appointment.userdetails.contact, true)
      .then(res => {
        console.log("Launched dialer!", res);
      })
      .catch(err => {
        console.log("Error launching dialer!", err);
      });
    }
    else
    {
      this.toast.show('No contact found!','3000','center').subscribe();
    }
  }

  uploadReports()
  {
    this.navCtrl.push(DoctorsReportRepositoryPage, {userkey: this.appointment.userkey});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewDoctorAppointmentsPage');
  }

}
