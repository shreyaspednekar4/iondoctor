import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPatientAppointmentsPage } from './view-patient-appointments';

@NgModule({
  declarations: [
    ViewPatientAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewPatientAppointmentsPage),
  ],
})
export class ViewPatientAppointmentsPageModule {}
