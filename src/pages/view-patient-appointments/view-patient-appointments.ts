import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-view-patient-appointments',
  templateUrl: 'view-patient-appointments.html',
})
export class ViewPatientAppointmentsPage {

  public appointment: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public callNumber: CallNumber,
              public toast: Toast) {

                this.appointment = navParams.get('appointment');
  }

  callNow(appointment)
  {
    console.log(appointment.doctordetails.contact);
    if(appointment.doctordetails.contact != null)
    {
      this.callNumber.callNumber(appointment.doctordetails.contact, true)
      .then(res => {
        console.log("Launched dialer!", res);
      })
      .catch(err => {
        console.log("Error launching dialer!", err);
      });
    }
    else
    {
      this.toast.show('No contact found!','3000','center').subscribe();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPatientAppointmentsPage');
  }

}
