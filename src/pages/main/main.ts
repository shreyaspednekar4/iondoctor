import { RegisterPage } from './../register/register';
import { HomePatientPage } from './../home-patient/home-patient';
import { HomeDoctorPage } from './../home-doctor/home-doctor';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  authForm: FormGroup;

  usertype: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public toastCtrl: ToastController,
              public fireAuth: AngularFireAuth,
              public alertCtrl: AlertController,
              public toast: Toast) {

                this.authForm = formBuilder.group({
                  logintype: ['', Validators.compose([Validators.required])],
                  email: ['', Validators.compose([Validators.required])],
                  password: ['', Validators.compose([Validators.required])]
                });
  }

  login()
  {
    let logintype   : string = this.authForm.controls["logintype"].value,
        email   : string = this.authForm.controls["email"].value,
        password   : string = this.authForm.controls["password"].value

        let loading = this.loadingCtrl.create({content : "Logging in, please wait..."});
        loading.present();

        this.fireAuth.auth.signInWithEmailAndPassword(email, password)
        .then((data) => {
          var ref = firebase.database().ref('users');
          ref.orderByChild("email").equalTo(email).once("value", (items : any) => {
            console.log("Key: ",items.key);
            console.log("Value: ",items.val());

            let users : any = [];

            items.forEach((item) =>
            {
              users.push({
                key              : item.key,
                usertype         : item.val().usertype,
                email            : item.val().email,
                password         : item.val().password,
                phone            : item.val().phone,
                fullname         : item.val().fullname
              });
              this.usertype = item.val().usertype;
              console.log(this.usertype);
              this.storage.set('userkey',item.key);
              this.storage.set('useremail',item.val().email);
              this.storage.set('usertype',item.val().usertype);
              this.storage.set('isUserLoggedIn','true');

              loading.dismissAll();

              if(logintype == this.usertype)
              {
                if(this.usertype == "Doctor")
                {
                  //this.toast.show('Successfully loged in! Please complete your profile first!','5000','center').subscribe();
                  this.navCtrl.push(HomeDoctorPage);
                }
                else if(this.usertype == "Patient")
                {
                  //this.toast.show('Successfully loged in! Please complete your profile first!','5000','center').subscribe();
                  this.navCtrl.push(HomePatientPage);
                }
              }
              else
              {
                this.toast.show('Incorrect user type!','3000','center').subscribe();
              }
            });
          });

        })
        .catch(error => {
          console.log("got an error", error);
          loading.dismissAll();
          this.alert(error.message);
        });
  }

  alert(message: string) {
    this.alertCtrl
      .create({
        title: "Info!",
        subTitle: message,
        buttons: ["OK"]
      })
      .present();
  }

  register()
  {
    this.navCtrl.push(RegisterPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

}
