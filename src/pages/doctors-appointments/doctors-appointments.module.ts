import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorsAppointmentsPage } from './doctors-appointments';

@NgModule({
  declarations: [
    DoctorsAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorsAppointmentsPage),
  ],
})
export class DoctorsAppointmentsPageModule {}
