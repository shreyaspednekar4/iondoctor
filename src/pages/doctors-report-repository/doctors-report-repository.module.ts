import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorsReportRepositoryPage } from './doctors-report-repository';

@NgModule({
  declarations: [
    DoctorsReportRepositoryPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorsReportRepositoryPage),
  ],
})
export class DoctorsReportRepositoryPageModule {}
