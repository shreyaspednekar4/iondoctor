import { ChangeAddressDetailsPage } from './../change-address-details/change-address-details';
import { ChangeDoctorDetailsPage } from './../change-doctor-details/change-doctor-details';
import { ChangePersonalDetailsPage } from './../change-personal-details/change-personal-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, ModalController } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';
import { DatabaseProvider } from '../../providers/database/database';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  userEmail: any;
  userKey: any;
  userData: any;
  userType: boolean;
  public displayImage: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              public actionSheetCtrl: ActionSheetController,
              public toastCtrl: ToastController,
              public dbProvider: DatabaseProvider,
              public camera: Camera,
              public photoViewer: PhotoViewer,
              public modalCtrl: ModalController) {

                this.getUserKey();
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.userKey = val;
      this.checkUserType();
      console.log("Storage Key: ",this.userKey);
      this.getUserData(this.userKey);
    });
  }

  checkUserType()
  {
    this.storage.get('usertype').then((val) => {
      if(val == "Doctor")
      {
        this.userType = true;
      }
      else if(val == "Patient")
      {
        this.userType = false;
      }
    })
  }

  getUserData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        },
        doctordetails   : {
          clinicname    : item.val().doctordetails.clinicname,
          education     : item.val().doctordetails.education,
          department    : item.val().doctordetails.department,
          speciality    : item.val().doctordetails.speciality,
          experience    : item.val().doctordetails.experience,
          fees          : item.val().doctordetails.fees,
          clinictimefrom: item.val().doctordetails.clinictimefrom,
          clinictimeto  : item.val().doctordetails.clinictimeto
        }
      })
      this.userKey = item.key;
      this.userData = user;
      console.log(this.userData);
    })
  }

  editPD(user)
  {
    //this.navCtrl.push(ChangePersonalDetailsPage, {user: user});

    let modal = this.modalCtrl.create('ChangePersonalDetailsPage', {user: user});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  editDD(user)
  {
    //this.navCtrl.push(ChangeDoctorDetailsPage, {user: user});

    let modal = this.modalCtrl.create('ChangeDoctorDetailsPage', {user: user});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  editAD(user)
  {
    //this.navCtrl.push(ChangeAddressDetailsPage, {user: user});

    let modal = this.modalCtrl.create('ChangeAddressDetailsPage', {user: user});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  getPicture()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load From Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType){

    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000
    };

    this.camera.getPicture(options).then((data) => {
        this.displayImage = 'data:image/jpg;base64,' + data;
        //alert("1: "+this.displayImage);
        this.uploadImage(this.displayImage);

    }, (err) => {
      //this.presentToast('Error while selecting image.');
    });
  }

  uploadImage(image)
  {
    this.dbProvider.uploadDisplayImage(image)
    .then((snapshot: any) => {
      let uploadPicture: any = snapshot.downloadURL;
      //alert("3: URL: "+uploadPicture);

      this.updateProfilePicture(this.userKey,
        {
          profile_picture: uploadPicture
        }
      );
      this.ionViewDidLoad();
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      this.toastCtrl.create({message: 'Profile Picture updated successfully!', duration: 2000}).present();
    })
    .catch(error => {
      this.toastCtrl.create({message: 'Something went wrong!', duration: 2000}).present();
    });
  }

  updateProfilePicture(id, userObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('customer_details').child(id);
	    updateRef.update(userObj);
      resolve(true);
    });
  }

  showPicture(user)
  {
    console.log(user.profile_picture);
    this.photoViewer.show(user.profile_picture, user.fullname, {share: true});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getUserKey();
  }

  ionViewWillEnter(){
    this.getUserKey();
  }

}
