import { AppInfoPage } from './../app-info/app-info';
import { ForgotPasswordPage } from './../forgot-password/forgot-password';
import { MainPage } from './../main/main';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'popover.html',
})
export class PopoverPage {

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public storage: Storage) {
  }

  logout()
  {
    let confirm = this.alertCtrl.create({
        title: 'Exit Application?',
        message: 'Click Yes to Exit !',
        buttons: [
          {
            text: 'NO',
          },
          {
            text: 'YES',
            handler: () => {
            console.log('YES clicked');
            this.storage.remove('userkey');
            this.storage.remove('useremail');
            this.storage.remove('usertype');
            this.storage.remove('isUserLoggedIn');
            this.navCtrl.push(MainPage);
          }
        }
      ]
    });
    confirm.present();
  }

  forgotPassword()
  {
    this.navCtrl.push(ForgotPasswordPage);
  }

  appInfo()
  {
    this.navCtrl.push(AppInfoPage);
  }

}
