import { ViewFeedbackPage } from './../view-feedback/view-feedback';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-manage-feedbacks',
  templateUrl: 'manage-feedbacks.html',
})
export class ManageFeedbacksPage {

  doctorkey: any;
  feedbacks: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage) {

                this.getUserKey();
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.doctorkey = val;
      //console.log("Storage Key: ",this.userkey);
      this.getFeedbacks(this.doctorkey);
    });
  }

  getFeedbacks(doctorkey)
  {
    var ref = firebase.database().ref('feedbacks');
    ref.orderByChild("doctorkey").equalTo(doctorkey).once("value", (items : any) => {
      let feedbacks = [];
      items.forEach((item) => {
        feedbacks.push({
          key             : item.key,
          userkey         : item.val().userkey,
          doctorkey       : item.val().doctorkey,
          recommend       : item.val().recommend,
          visitreasons    : item.val().visitreasons,
          time            : item.val().time,
          behaviours      : item.val().behaviours,
          experience      : item.val().experience,
          date            : item.val().date,
          userdetails: {
            fullname        : item.val().userdetails.fullname,
            contact         : item.val().userdetails.contact,
            email           : item.val().userdetails.email,
            age             : item.val().userdetails.age,
            gender          : item.val().userdetails.gender,
            profile_picture : item.val().userdetails.profile_picture,
            address: {
              addline1      : item.val().userdetails.address.addline1,
              addline2      : item.val().userdetails.address.addline2,
              country       : item.val().userdetails.address.country,
              state         : item.val().userdetails.address.state,
              city          : item.val().userdetails.address.city,
              postal_code   : item.val().userdetails.address.postal_code
            }
          },
          doctordetails: {
            fullname        : item.val().doctordetails.fullname,
            contact         : item.val().doctordetails.contact,
            email           : item.val().doctordetails.email,
            age             : item.val().doctordetails.age,
            gender          : item.val().doctordetails.gender,
            profile_picture : item.val().doctordetails.profile_picture,
            address: {
              addline1      : item.val().doctordetails.address.addline1,
              addline2      : item.val().doctordetails.address.addline2,
              country       : item.val().doctordetails.address.country,
              state         : item.val().doctordetails.address.state,
              city          : item.val().doctordetails.address.city,
              postal_code   : item.val().doctordetails.address.postal_code
            },
            clinicdetails   : {
              clinicname    : item.val().doctordetails.clinicdetails.clinicname,
              education     : item.val().doctordetails.clinicdetails.education,
              department    : item.val().doctordetails.clinicdetails.department,
              speciality    : item.val().doctordetails.clinicdetails.speciality,
              experience    : item.val().doctordetails.clinicdetails.experience,
              fees          : item.val().doctordetails.clinicdetails.fees,
              clinictimefrom: item.val().doctordetails.clinicdetails.clinictimefrom,
              clinictimeto  : item.val().doctordetails.clinicdetails.clinictimeto
            }
          }
        });
      });
      this.feedbacks = feedbacks;
      console.log(this.feedbacks);
    })
  }

  viewFeedback(feedback)
  {
    this.navCtrl.push(ViewFeedbackPage, {feedback: feedback});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManageFeedbacksPage');
    this.getUserKey();
  }

  ionViewWillEnter(){
    this.getUserKey();
  }

}
