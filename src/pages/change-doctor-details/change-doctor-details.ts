import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireList } from "angularfire2/database"
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-change-doctor-details',
  templateUrl: 'change-doctor-details.html',
})
export class ChangeDoctorDetailsPage {

  authForm: FormGroup;

  public user: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public toast: Toast,
              public viewCtrl: ViewController) {

                this.user = navParams.get('user');

                this.authForm = formBuilder.group({
                  clinicname: [''],
                  education: ['', Validators.compose([Validators.required])],
                  department: [''],
                  speciality: ['', Validators.compose([Validators.required])],
                  experience: ['', Validators.compose([Validators.required])],
                  fees: [''],
                  clinictimefrom: [''],
                  clinictimeto: ['']
              });
  }

  save()
  {
    let clinicname   : string = this.authForm.controls["clinicname"].value,
        education    : string = this.authForm.controls["education"].value,
        department      : string = this.authForm.controls["department"].value,
        speciality        : string = this.authForm.controls["speciality"].value,
        experience        : string = this.authForm.controls["experience"].value,
        fees        : string = this.authForm.controls["fees"].value,
        clinictimefrom     : string = this.authForm.controls["clinictimefrom"].value,
        clinictimeto     : string = this.authForm.controls["clinictimeto"].value

        this.updateUser(this.user['key'],
        {
          doctordetails: {
            clinicname: clinicname,
            education: education,
            department: department,
            speciality: speciality,
            experience: experience,
            fees: fees,
            clinictimefrom: clinictimefrom,
            clinictimeto: clinictimeto
          }
        });

        //this.toastCtrl.create({message: 'Details updated successfully!', duration: 3000}).present();
        this.toast.show('Details updated successfully!','3000','center').subscribe();
        this.viewCtrl.dismiss();
  }

  updateUser(id, userObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('users').child(id);
	    updateRef.update(userObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeDoctorDetailsPage');
  }

}
