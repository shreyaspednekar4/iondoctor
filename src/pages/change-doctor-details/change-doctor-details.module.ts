import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeDoctorDetailsPage } from './change-doctor-details';

@NgModule({
  declarations: [
    ChangeDoctorDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeDoctorDetailsPage),
  ],
})
export class ChangeDoctorDetailsPageModule {}
