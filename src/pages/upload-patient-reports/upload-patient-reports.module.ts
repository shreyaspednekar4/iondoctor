import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadPatientReportsPage } from './upload-patient-reports';

@NgModule({
  declarations: [
    UploadPatientReportsPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadPatientReportsPage),
  ],
})
export class UploadPatientReportsPageModule {}
