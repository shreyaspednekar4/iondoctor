import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientPrescriptionsPage } from './patient-prescriptions';

@NgModule({
  declarations: [
    PatientPrescriptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientPrescriptionsPage),
  ],
})
export class PatientPrescriptionsPageModule {}
