import { ViewPatientPrescriptionPage } from './../view-patient-prescription/view-patient-prescription';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-patient-prescriptions',
  templateUrl: 'patient-prescriptions.html',
})
export class PatientPrescriptionsPage {

  public appointments: any;
  public patient: any;
  public doctor: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage) {
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      console.log("Storage Key: ",val);
      this.getAppointments(val);
    });
  }

  getAppointments(userkey)
  {
    var ref = firebase.database().ref("appointments");
    ref.orderByChild("userkey").equalTo(userkey).once("value", (items : any) => {
      let appointments : any = [];
      items.forEach((item) => {
        appointments.push({
          key             : item.key,
          userkey         : item.val().userkey,
          doctorkey       : item.val().doctorkey,
          patientrelation : item.val().patientrelation,
          patientname     : item.val().patientname,
          patientgender   : item.val().patientgender,
          patientage      : item.val().patientage,
          appointmentdate : item.val().appointmentdate,
          patientissue    : item.val().patientissue,
          appointmenttime : item.val().appointmenttime,
          checkuptype     : item.val().checkuptype,
          userdetails     : {
            userkey         : item.val().userdetails.userkey,
            fullname        : item.val().userdetails.fullname,
	          contact         : item.val().userdetails.contact,
	          email           : item.val().userdetails.email,
            password        : item.val().userdetails.password,
            age             : item.val().userdetails.age,
            gender          : item.val().userdetails.gender,
            profile_picture : item.val().userdetails.profile_picture,
          },
          doctordetails    : {
            doctorkey       : item.val().doctordetails.doctorkey,
            fullname        : item.val().doctordetails.fullname,
	          contact         : item.val().doctordetails.contact,
	          email           : item.val().doctordetails.email,
            password        : item.val().doctordetails.password,
            age             : item.val().doctordetails.age,
            gender          : item.val().doctordetails.gender,
            profile_picture : item.val().doctordetails.profile_picture,
            doctoraddress         : {
              addline1      : item.val().doctordetails.doctoraddress.addline1,
              addline2      : item.val().doctordetails.doctoraddress.addline2,
              country       : item.val().doctordetails.doctoraddress.country,
              state         : item.val().doctordetails.doctoraddress.state,
              city          : item.val().doctordetails.doctoraddress.city,
              postal_code   : item.val().doctordetails.doctoraddress.postal_code
            },
            clinicdetails   : {
              clinicname    : item.val().doctordetails.clinicdetails.clinicname,
              education     : item.val().doctordetails.clinicdetails.education,
              department    : item.val().doctordetails.clinicdetails.department,
              speciality    : item.val().doctordetails.clinicdetails.speciality,
              experience    : item.val().doctordetails.clinicdetails.experience,
              fees          : item.val().doctordetails.clinicdetails.fees,
              clinictimefrom: item.val().doctordetails.clinicdetails.clinictimefrom,
              clinictimeto  : item.val().doctordetails.clinicdetails.clinictimeto
            }
          }
        });
      });
      this.appointments = appointments.reverse();
      console.log(this.appointments);
    });
  }

  view(app)
  {
    this.navCtrl.push(ViewPatientPrescriptionPage, {appointment: app});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientPrescriptionsPage');
    this.getUserKey();
  }

  ionViewWillEnter(){
    this.getUserKey();
  }

}
