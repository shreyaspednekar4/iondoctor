import { MainPage } from './../main/main';
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  authForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public toastCtrl: ToastController,
              public fireAuth: AngularFireAuth,
              public alertCtrl: AlertController,
              public fdb: AngularFireDatabase,
              public toast: Toast) {

                this.authForm = formBuilder.group({
                  logintype: ['', Validators.compose([Validators.required])],
                  fullname: ['', Validators.compose([Validators.required])],
                  contact: ['', Validators.compose([Validators.required])],
                  email: ['', Validators.compose([Validators.required])],
                  password: ['', Validators.compose([Validators.required])]
                });
  }

  login()
  {
    this.navCtrl.push(MainPage);
  }

  register()
  {
    let logintype   : string = this.authForm.controls["logintype"].value,
        fullname   : string = this.authForm.controls["fullname"].value,
        contact   : string = this.authForm.controls["contact"].value,
        email   : string = this.authForm.controls["email"].value,
        password   : string = this.authForm.controls["password"].value

        let loading = this.loadingCtrl.create({content : "Creating account, please wait..."});
        loading.present();

        this.fireAuth.auth.createUserWithEmailAndPassword(email, password)
        .then((data) => {
          this.fdb.list("/users/").push({
            usertype: logintype,
            fullname: fullname,
            contact: contact,
            email: email,
            password: password,
            profile_picture: 'https://firebasestorage.googleapis.com/v0/b/doctorsapp-a1deb.appspot.com/o/doctor1.png?alt=media&token=aeec846b-e2d0-4bfe-b402-d85f730ea5c4',
            age: '',
            gender: '',
            address: {
              addline1: '',
              addline2: '',
              country: '',
              state: '',
              city: '',
              postal_code: ''
            },
            doctordetails: {
              clinicname: '',
              education: '',
              department: '',
              speciality: '',
              experience: '',
              fees: '',
              clinictimefrom: '',
              clinictimeto: ''
            }
          });
          //console.log("DATA: ",data);

          loading.dismissAll();
          this.toast.show('Account created successfully! Please login into your account!','5000','center').subscribe();
          this.navCtrl.push(MainPage);
        })
        .catch((error) => {
          console.log("ERROR: ",error);
          loading.dismissAll();
          this.alert(error.message);
        });
  }

  alert(message: string) {
    this.alertCtrl
      .create({
        title: "Info!",
        subTitle: message,
        buttons: ["OK"]
      })
      .present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
