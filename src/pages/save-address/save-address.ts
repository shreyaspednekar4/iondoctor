import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-save-address',
  templateUrl: 'save-address.html',
})
export class SaveAddressPage {

  current: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {

                this.current = navParams.get('current');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SaveAddressPage');
  }

}
