import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaveAddressPage } from './save-address';

@NgModule({
  declarations: [
    SaveAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(SaveAddressPage),
  ],
})
export class SaveAddressPageModule {}
