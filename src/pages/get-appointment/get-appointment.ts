import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-get-appointment',
  templateUrl: 'get-appointment.html',
})
export class GetAppointmentPage {

  authForm: FormGroup;

  doctor: any;
  userKey: any;
  userData: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public toastCtrl: ToastController,
              public fireAuth: AngularFireAuth,
              public alertCtrl: AlertController,
              public fdb: AngularFireDatabase,
              public toast: Toast,
              public viewCtrl: ViewController) {

                this.getUserKey();

                this.doctor = navParams.get('doctor');
                console.log("Doctor: ",this.doctor);

                this.authForm = formBuilder.group({
                  patientrelation: ['', Validators.compose([Validators.required])],
                  patientname: ['', Validators.compose([Validators.required])],
                  patientgender: ['', Validators.compose([Validators.required])],
                  patientage: ['', Validators.compose([Validators.required])],
                  appointmentdate: ['', Validators.compose([Validators.required])],
                  checkuptype: ['', Validators.compose([Validators.required])],
                  patientissue: ['']
                });
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      console.log("Storage Key: ",val);
      this.getUserData(val);
    });
  }

  getUserData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        }
      })
      //this.userKey = item.key;
      this.userData = user;
      console.log("User: ",this.userData);
    })
  }

  selectRelation(relation)
  {
    if(relation != "Myself")
    {
      this.authForm.controls["patientname"].reset();
    }
  }

  submit()
  {
    let confirm = this.alertCtrl.create({
        title: 'Book an Appointment?',
        message: 'Your appointment time will be 07:00',
        buttons: [
          {
            text: 'Cancel',
          },
          {
            text: 'Ok! Book',
            handler: () => {
              this.bookAppointment();
              //this.toastCtrl.create({message: 'Appointment booked successfully!', duration: 2000}).present();
              this.toast.show('Appointment booked successfully!','3000','center').subscribe();
              this.viewCtrl.dismiss();
          }
        }
      ]
    });
    confirm.present();
  }

  bookAppointment()
  {
    let patientrelation   : string = this.authForm.controls["patientrelation"].value,
        patientname   : string = this.authForm.controls["patientname"].value,
        patientgender   : string = this.authForm.controls["patientgender"].value,
        patientage   : string = this.authForm.controls["patientage"].value,
        appointmentdate   : string = this.authForm.controls["appointmentdate"].value,
        checkuptype   : string = this.authForm.controls["checkuptype"].value,
        patientissue   : string = this.authForm.controls["patientissue"].value

    this.saveAppointment({
      userkey: this.userData[0].key,
      doctorkey: this.doctor.key,
      patientrelation: patientrelation,
      patientname: patientname,
      patientgender: patientgender,
      patientage: patientage,
      appointmentdate: appointmentdate,
      checkuptype: checkuptype,
      patientissue: patientissue,
      appointmenttime: '07:00',
      userdetails: {
        userkey: this.userData[0].key,
        fullname: this.userData[0].fullname,
	      contact: this.userData[0].contact,
	      email: this.userData[0].email,
        password: this.userData[0].password,
        age: this.userData[0].age,
        gender: this.userData[0].gender,
        profile_picture: this.userData[0].profile_picture,
      },
      doctordetails: {
        doctorkey: this.doctor.key,
        fullname: this.doctor.fullname,
	      contact: this.doctor.contact,
	      email: this.doctor.email,
        password: this.doctor.password,
        age: this.doctor.age,
        gender: this.doctor.gender,
        profile_picture : this.doctor.profile_picture,
        doctoraddress: {
          addline1: this.doctor.address.addline1,
          addline2: this.doctor.address.addline2,
          country: this.doctor.address.country,
          state: this.doctor.address.state,
          city: this.doctor.address.city,
          postal_code: this.doctor.address.postal_code
        },
        clinicdetails: {
          clinicname: this.doctor.doctordetails.clinicname,
          education: this.doctor.doctordetails.education,
          department: this.doctor.doctordetails.department,
          speciality: this.doctor.doctordetails.speciality,
          experience: this.doctor.doctordetails.experience,
          fees: this.doctor.doctordetails.fees,
          clinictimefrom: this.doctor.doctordetails.clinictimefrom,
          clinictimeto: this.doctor.doctordetails.clinictimeto
        }
      }
    });
  }

  saveAppointment(appObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('appointments');
	    updateRef.push(appObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GetAppointmentPage');
    this.getUserKey();
  }

  ionViewWillEnter(){
    this.getUserKey();
  }

}
