import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetAppointmentPage } from './get-appointment';

@NgModule({
  declarations: [
    GetAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(GetAppointmentPage),
  ],
})
export class GetAppointmentPageModule {}
