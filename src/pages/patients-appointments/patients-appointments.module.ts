import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsAppointmentsPage } from './patients-appointments';

@NgModule({
  declarations: [
    PatientsAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientsAppointmentsPage),
  ],
})
export class PatientsAppointmentsPageModule {}
