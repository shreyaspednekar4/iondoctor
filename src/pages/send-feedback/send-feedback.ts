import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";

@IonicPage()
@Component({
  selector: 'page-send-feedback',
  templateUrl: 'send-feedback.html',
})
export class SendFeedbackPage {

  authForm: FormGroup;
  userkey: any;
  userdata: any;
  doctordata: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public viewCtrl: ViewController) {

                this.doctordata = navParams.get('doctor');
                console.log("Doctor: ",this.doctordata);
                this.getUserKey();

                this.authForm = formBuilder.group({
                  recommend: ['', Validators.compose([Validators.required])],
                  visitreason: ['', Validators.compose([Validators.required])],
                  time: ['', Validators.compose([Validators.required])],
                  behaviour: ['', Validators.compose([Validators.required])],
                  experience: ['']
                });
  }

  getUserKey()
  {
    this.storage.get('userkey').then((val) => {
      this.userkey = val;
      //console.log("Storage Key: ",this.userkey);
      this.getUserData(this.userkey);
    });
  }

  getUserData(userkey)
  {
    var ref = firebase.database().ref('users');
    ref.child(userkey).once("value", (item: any) => {
      let user = [];
      user.push({
        key             : item.key,
	      usertype        : item.val().usertype,
	      fullname        : item.val().fullname,
	      contact         : item.val().contact,
	      email           : item.val().email,
        password        : item.val().password,
        age             : item.val().age,
        gender          : item.val().gender,
        profile_picture : item.val().profile_picture,
        address         : {
          addline1      : item.val().address.addline1,
          addline2      : item.val().address.addline2,
          country       : item.val().address.country,
          state         : item.val().address.state,
          city          : item.val().address.city,
          postal_code   : item.val().address.postal_code
        },
        doctordetails   : {
          clinicname    : item.val().doctordetails.clinicname,
          education     : item.val().doctordetails.education,
          department    : item.val().doctordetails.department,
          speciality    : item.val().doctordetails.speciality,
          experience    : item.val().doctordetails.experience,
          fees          : item.val().doctordetails.fees,
          clinictimefrom: item.val().doctordetails.clinictimefrom,
          clinictimeto  : item.val().doctordetails.clinictimeto
        }
      })
      this.userkey = item.key;
      this.userdata = user;
      console.log(this.userdata);
    })
  }

  submit()
  {
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    var today_date = curr_date + "/" + curr_month + "/" + curr_year;

    let recommend: string = this.authForm.controls["recommend"].value,
        visitreason: any = this.authForm.controls["visitreason"].value,
        time: string = this.authForm.controls["time"].value,
        behaviour: any = this.authForm.controls["behaviour"].value,
        experience: string = this.authForm.controls["experience"].value,
        visitreasons: any = [],
        behaviours: any = [],
        k: any;

        for(k in visitreason)
        {
          visitreasons.push({
            "reason": visitreason[k]
          });
        }

        for(k in behaviour)
        {
          behaviours.push({
            "behaviour": behaviour[k]
          });
        }

        this.saveFeedback({
          userkey: this.userdata[0].key,
          doctorkey: this.doctordata.key,
          recommend: recommend,
          visitreasons: visitreasons,
          time: time,
          behaviours: behaviours,
          experience: experience,
          date: today_date,
          userdetails: {
            fullname: this.userdata[0].fullname,
            contact: this.userdata[0].contact,
            email: this.userdata[0].email,
            age: this.userdata[0].age,
            gender: this.userdata[0].gender,
            profile_picture: this.userdata[0].profile_picture,
            address: {
              addline1: this.userdata[0].address.addline1,
              addline2: this.userdata[0].address.addline2,
              country: this.userdata[0].address.country,
              state: this.userdata[0].address.state,
              city: this.userdata[0].address.city,
              postal_code: this.userdata[0].address.postal_code
            }
          },
          doctordetails: {
            fullname: this.doctordata.fullname,
            contact: this.doctordata.contact,
            email: this.doctordata.email,
            age: this.doctordata.age,
            gender: this.doctordata.gender,
            profile_picture: this.doctordata.profile_picture,
            address: {
              addline1: this.doctordata.address.addline1,
              addline2: this.doctordata.address.addline2,
              country: this.doctordata.address.country,
              state: this.doctordata.address.state,
              city: this.doctordata.address.city,
              postal_code: this.doctordata.address.postal_code
            },
            clinicdetails: {
              clinicname: this.doctordata.doctordetails.clinicname,
              education: this.doctordata.doctordetails.education,
              department: this.doctordata.doctordetails.department,
              speciality: this.doctordata.doctordetails.speciality,
              experience: this.doctordata.doctordetails.experience,
              fees: this.doctordata.doctordetails.fees,
              clinictimefrom: this.doctordata.doctordetails.clinictimefrom,
              clinictimeto: this.doctordata.doctordetails.clinictimeto
            }
          }
        });
        let toast = this.toastCtrl.create({
          message: 'Feedback sent successfully!',
          duration: 2000
        });
        toast.present();
        this.viewCtrl.dismiss();
  }

  saveFeedback(repObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('feedbacks');
	    updateRef.push(repObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SendFeedbackPage');
    this.getUserKey();
  }

}
