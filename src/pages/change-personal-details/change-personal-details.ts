import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireList } from "angularfire2/database"
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-change-personal-details',
  templateUrl: 'change-personal-details.html',
})
export class ChangePersonalDetailsPage {

  authForm: FormGroup;

  public user: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public toast: Toast,
              public viewCtrl: ViewController) {

                this.user = navParams.get('user');

                this.authForm = formBuilder.group({
                  fullname: ['', Validators.compose([Validators.required])],
                  contact: ['', Validators.compose([Validators.required])],
                  email: [''],
                  age: ['', Validators.compose([Validators.required])],
                  gender: ['', Validators.compose([Validators.required])]
              });
  }

  save()
  {
    let fullname   : string = this.authForm.controls["fullname"].value,
        contact    : string = this.authForm.controls["contact"].value,
        email      : string = this.authForm.controls["email"].value,
        age        : string = this.authForm.controls["age"].value,
        gender     : string = this.authForm.controls["gender"].value

        this.updateUser(this.user['key'],
        {
          fullname: fullname,
          contact: contact,
          email: email,
          age: age,
          gender: gender
        });

        //this.toastCtrl.create({message: 'Personal details updated successfully!', duration: 3000}).present();

        this.toast.show('Personal details updated successfully!','3000','center').subscribe();
        this.viewCtrl.dismiss();
  }

  updateUser(id, userObj) : Promise<any>
  {
    return new Promise((resolve) =>
    {
      var updateRef = firebase.database().ref('users').child(id);
	    updateRef.update(userObj);
      resolve(true);
    });
  }

  cancel()
  {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePersonalDetailsPage');
  }

}
