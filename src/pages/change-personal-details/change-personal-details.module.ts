import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangePersonalDetailsPage } from './change-personal-details';

@NgModule({
  declarations: [
    ChangePersonalDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangePersonalDetailsPage),
  ],
})
export class ChangePersonalDetailsPageModule {}
