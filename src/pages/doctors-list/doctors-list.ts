import { GetAppointmentPage } from './../get-appointment/get-appointment';
import { ViewDoctorProfilePage } from './../view-doctor-profile/view-doctor-profile';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import * as firebase from "firebase/app";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireList } from "angularfire2/database";
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-doctors-list',
  templateUrl: 'doctors-list.html',
})
export class DoctorsListPage {

  public doctorlist: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public callNumber: CallNumber,
              public modalCtrl: ModalController) {

                this.getDoctors();
  }

  getDoctors()
  {
    var ref = firebase.database().ref("users");
    ref.orderByChild("usertype").equalTo("Doctor").once("value", (items : any) => {
      let doctors : any = [];
      items.forEach((item) => {
        doctors.push({
          key             : item.key,
          usertype        : item.val().usertype,
          fullname        : item.val().fullname,
          contact         : item.val().contact,
          email           : item.val().email,
          password        : item.val().password,
          age             : item.val().age,
          gender          : item.val().gender,
          profile_picture : item.val().profile_picture,
          address         : {
            addline1      : item.val().address.addline1,
            addline2      : item.val().address.addline2,
            country       : item.val().address.country,
            state         : item.val().address.state,
            city          : item.val().address.city,
            postal_code   : item.val().address.postal_code
          },
          doctordetails   : {
            clinicname    : item.val().doctordetails.clinicname,
            education     : item.val().doctordetails.education,
            department    : item.val().doctordetails.department,
            speciality    : item.val().doctordetails.speciality,
            experience    : item.val().doctordetails.experience,
            fees          : item.val().doctordetails.fees,
            clinictimefrom: item.val().doctordetails.clinictimefrom,
            clinictimeto  : item.val().doctordetails.clinictimeto
          }
        })
      });
      this.doctorlist = doctors;
      console.log(this.doctorlist);
    });
  }

  view(user)
  {
    this.navCtrl.push(ViewDoctorProfilePage, {user : user});
  }

  callNow(doctor)
  {
    console.log(doctor.contact);
    if(doctor.contact != null)
    {
      this.callNumber.callNumber(doctor.contact, true)
      .then(res => {
        console.log("Launched dialer!", res);
      })
      .catch(err => {
        console.log("Error launching dialer!", err);
      });
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Oops',
        message: 'No contact found!',
        buttons: [
          {
            text: 'Ok'
          }
        ]
      });
      alert.present();
    }
  }

  gotoGetAppointment(doctor)
  {
    console.log(doctor);
    //this.navCtrl.push(GetAppointmentPage, {doctor: doctor});

    let modal = this.modalCtrl.create('GetAppointmentPage', {doctor: doctor});
    modal.present();

    modal.onWillDismiss(() => {
    })
  }

  gotoReportRepository(doctor)
  {
    console.log(doctor.key);

    // let modal = this.modalCtrl.create('GetAppointmentPage', {doctorkey: doctor.key});
    // modal.present();

    // modal.onWillDismiss(() => {
    // })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorsListPage');
    this.getDoctors();
  }

  ionViewWillEnter(){
    this.getDoctors();
  }

}
