import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewDoctorPrescriptionPage } from './view-doctor-prescription';

@NgModule({
  declarations: [
    ViewDoctorPrescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewDoctorPrescriptionPage),
  ],
})
export class ViewDoctorPrescriptionPageModule {}
